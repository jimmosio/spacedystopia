﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrapAttraction : MovingThing {
    private GameObject playerObject;

    private bool attracted = false;

    public float PickUpRange = 5.0f;
    public float BaseThrust = 10.0f;
    public float CriticalDistance = 10.0f;

    // Use this for initialization
    public new void Start () {
        GetComponent<MovingThing>().Start();

        playerObject = GameObject.FindWithTag("Player");

    }

    private void OnTriggerEnter(Collider other)
    {
        //Find the object you collided with
        #region
        GameObject obj;
        //Collision is done with the 'bodies' of the ship, but the meat sacks and stuff are in the parent object, so we gotta retrieve that

        Transform ptr = other.gameObject.transform.parent; //Parent's transform. It's a weird way of doing it but it's on Google so

        //If there's no such thing as a "parent's transform", it means that the object is a parent itself
        if (ptr != null)
        {
            obj = ptr.gameObject;
        }
        else
        {
            obj = other.gameObject;
        }
        #endregion

        if (obj.tag == "Player")
        {
            Destroy(gameObject);
            obj.GetComponent<PlayerShip>().meatsack.GetDamaged(-1.0f);

        }
    }

    // Update is called once per frame
    public new void Update () {
        GetComponent<MovingThing>().Update();

        if (!attracted)
        {
            float disfromplayer = Vector3.Distance(transform.position, playerObject.transform.position);

            if (disfromplayer <= PickUpRange)
                attracted = true;
        }
        else
        {
            /*
            Rigidbody rb = GetComponent<Rigidbody>();

            Vector3 dir = (playerObject.transform.position - transform.position).normalized;

            rb.AddForce(AttractionForce * dir);
            */
            float dis = Vector3.Distance(playerObject.transform.position, transform.position);
            engine.ThrustForce = Mathf.Max(BaseThrust, BaseThrust * (CriticalDistance * CriticalDistance) / (dis * dis));
            engine.MoveThing(playerObject.transform.position, true, false, true);
        }
	}
}
