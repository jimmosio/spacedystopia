﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
//enum WepID {StandardLaser, EnemyLaser };

[System.Serializable]
public class Weapon
{
    //WepID ID;

    public static implicit operator WeaponSlot(Weapon w)
    {
        WeaponSlot wpnslt = new WeaponSlot();
        wpnslt.WepOfReference = w;

        return wpnslt;
    }

    public GameObject Prefab;// = (GameObject)Resources.Load("/Prefabs/projectiletest");
    public float ProjSpeed = 70.0f;
    public bool SpeedInheritance = false;
    public float LifeTime = 5.0f;
    public float BaseDamage = 10.0f;
    public float Inaccuracy = 0.0f;
    public float MaxCD = 0.25f;

}

public class AllWeapons : SceneSingleton<AllWeapons> {

    public Weapon StandardLaser = new Weapon
    {

    };

    public Weapon EnemyLaser = new Weapon
    {

    };


    // Use this for initialization
    void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
