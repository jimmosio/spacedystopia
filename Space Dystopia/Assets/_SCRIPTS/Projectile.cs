﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MovingThing
{
    private Rigidbody rb;

    //public float ProjSpeed;
    [HideInInspector]
    public float ProjDamage;

    //public float lifetime = 5.0f;

    [HideInInspector]
    public int owner;

    private void OnTriggerEnter(Collider other)
    {
        //Instances with this tag will spare the projectile from its doom
        List<string> exceptions = new List<string>() {"Scrap", "Particle"}; 

        //Damage what you collided with (if possible) and disappear
        #region

        //Find the object you collided with
        #region
        GameObject obj;
        //Collision is done with the 'bodies' of the ship, but the meat sacks and stuff are in the parent object, so we gotta retrieve that

        Transform ptr = other.gameObject.transform.parent; //Parent's transform. It's a weird way of doing it but it's on Google so

            //If there's no such thing as a "parent's transform", it means that the object is a parent itself
            if (ptr != null)
            {
                obj = ptr.gameObject;
            }
            else
            {
                obj = other.gameObject;
            }
        #endregion

        //The projectile mustn't harm the owner
        #region
        if (obj.GetInstanceID() == owner)
        {
            return;

        }
        #endregion

        //Find the meatsack, if it's there (we might be hitting a wall)
        #region
        MeatSack mtsck = null;
        //I tried doing it with GetComponent<GenericShip>() to make things tidier but it won't work.
        //It's because the player and the enemy actually have PlayerShip and EnemyShip respectively I guess
        if (obj.tag == "Player")
            {
                mtsck = obj.GetComponent<PlayerShip>().meatsack;

            }
            else if (obj.tag == "Enemy")
            {
                mtsck = obj.GetComponent<EnemyShip>().meatsack;

            }
        #endregion

        //Finally, damage the poor meatsack
        #region
        if (mtsck != null)
        {
            mtsck.GetDamaged(ProjDamage);

        }
        #endregion

        //It's time to go to Projectile Heaven (with some exceptions)
        if (!exceptions.Contains(obj.tag))
            Destroy(gameObject);
        #endregion
    }

    // Use this for initialization
    public new void Start () {
        GetComponent<MovingThing>().Start();
        rb = GetComponent<Rigidbody>();

        //Start moving
        //rb.velocity = rb.velocity + transform.forward * ProjSpeed;

        //Projectile will destroy itself after 'lifetime' seconds
        //Destroy(gameObject, lifetime);
    }
	
	// Update is called once per frame
	public new void Update () {
        GetComponent<MovingThing>().Update();

    }

    public new void FixedUpdate()
    {
        GetComponent<MovingThing>().FixedUpdate();

    }
}
