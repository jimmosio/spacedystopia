﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor.VersionControl;

[System.Serializable]
public class MeatSack
{
    [HideInInspector]
    public GameObject gameObject;

    public GameObject ScrapPrefab;

    public float MaxHealth;

    public float MaxShield;
    public float ShieldFixCooldown;
    public float ShieldBreakCooldown;
    public float ShieldRecharge;
    
    public float MaxEnergy;

    public GameObject ExplosionPrefab;

    [HideInInspector]
    public float Health;
    [HideInInspector]
    public float Shield;
    [HideInInspector]
    public float ShieldCurrCooldown;
    [HideInInspector]
    public float Energy;

    public void GetDamaged(float damage)
    {
        Health = Health - damage;

        if (Health <= 0.0f)
        {
            Die();
        }
    }

    public virtual void Die()
    {

        if (gameObject.tag == "Enemy")
        {
            //drop scrap
            for (int i = 0; i<3; i++)
            { //(GameObject)Resources.Load("Prefabs/Test/Scrap.prefab")
                GameObject scrp = Object.Instantiate(ScrapPrefab, gameObject.transform.position, Random.rotation) as GameObject;

                float a = 5.0f;

                scrp.GetComponent<Rigidbody>().velocity = new Vector3(Random.Range(-a, a), 0.0f, Random.Range(-a, a));
            }

            Object.Instantiate(ExplosionPrefab,gameObject.transform.position,Quaternion.identity);
            Object.Destroy(gameObject);

        }

        if (gameObject.tag == "Player")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        }
    }
}

[System.Serializable]
public class WeaponSlot
{
    public Weapon WepOfReference; //should be taken from the AllWeapons singleton

    [HideInInspector]
    public float CurrCD = 0.0f; //,CurrMaxCD
}

public class GenericShip : MovingThing {
    
    public MeatSack meatsack;

    [HideInInspector]
    public List<WeaponSlot> Inventory = new List<WeaponSlot>();

    public List<Vector3> Barrels;
    [HideInInspector]
    public int barrelamt, currbarrel;

    [HideInInspector]
    public GameObject newWeaponProj;
    
    public void Shoot(int slot_index)
    {
        //Volatile, changing variables (like current cooldown) are in WEPSLOT, while static ones (like base damage) are in WEP
        WeaponSlot WEPSLOT = Inventory[slot_index];
        Weapon WEP = WEPSLOT.WepOfReference;
        Rigidbody rb = GetComponent<Rigidbody>();

        if ((WEP.Prefab != null ) && (WEPSLOT.CurrCD == 0.0f))
        {
            //get position of the barrel
            Vector3 vec = Barrels[currbarrel];

            //get where the WeaponProjectile will be shot from
            Vector3 WeaponProjpos;
            WeaponProjpos = transform.position + transform.rotation * vec;

            //add inaccuracy, make sure the WeaponProjectiles lie on the XZ plane
            Quaternion inacc = Quaternion.AngleAxis(Random.Range(-WEP.Inaccuracy, WEP.Inaccuracy), transform.up);

            Quaternion straighten = new Quaternion();
            straighten.SetFromToRotation(transform.up, Vector3.up); //rotation that straightens the WeaponProjectiles to the XZ plane

            Quaternion rot = straighten * inacc * transform.rotation;

            //finally, shoot the damn thing
            newWeaponProj = Instantiate(WEP.Prefab, WeaponProjpos, rot);

            //Set various parameters
            #region
            //Velocity
            newWeaponProj.GetComponent<Rigidbody>().velocity = transform.forward * WEP.ProjSpeed;
            if (WEP.SpeedInheritance)
                {
                newWeaponProj.GetComponent<Rigidbody>().velocity = newWeaponProj.GetComponent<Rigidbody>().velocity + rb.velocity;

                }

            //Owner ID
            newWeaponProj.GetComponent<Projectile>().owner = gameObject.GetInstanceID();

            //Damage
            newWeaponProj.GetComponent<Projectile>().ProjDamage = WEP.BaseDamage;

            //Lifetime
            Destroy(newWeaponProj, WEP.LifeTime);
            #endregion

            //next barrel, set cooldown
            currbarrel = (currbarrel + 1) % barrelamt;
            WEPSLOT.CurrCD = WEP.MaxCD;
        }

    }

    // Use this for initialization
    public new void Start () {
        GetComponent<MovingThing>().Start();

        Rigidbody rb = GetComponent<Rigidbody>();

        //(DISABLED) Freeze ships on the XZ plane, make them not rotate around the X axis (look up/down)
        #region
        //There's code that forces this as well and the built-in freezing is inconsistent so this is commented out
        //rb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX;
        #endregion

        //Meatsack
        #region
        meatsack.gameObject = gameObject;

        meatsack.Health = meatsack.MaxHealth;
        meatsack.Shield = meatsack.MaxShield;
        meatsack.Energy = meatsack.MaxEnergy;
        #endregion

        barrelamt = Barrels.Capacity;
        currbarrel = 0;
        
    }

    // Update is called once per frame
    public new void Update () {
        GetComponent<MovingThing>().Update();

        //tick cooldown of weapons
        for (int i = 0; i < Inventory.Count; i++)
        {
            WeaponSlot wpnslt = Inventory[i];
            wpnslt.CurrCD = Mathf.Max(wpnslt.CurrCD - Time.deltaTime, 0.0f);
        }

        //Clamp health, shield and energy
        meatsack.Health = Mathf.Clamp(meatsack.Health, 0.0f, meatsack.MaxHealth);
        meatsack.Shield = Mathf.Clamp(meatsack.Shield, 0.0f, meatsack.MaxShield);
        meatsack.Energy = Mathf.Clamp(meatsack.Energy, 0.0f, meatsack.MaxEnergy);
    }
}
