﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : GenericShip {
    [HideInInspector]
    public GameObject player;

    public float AI_boostrange;
    public float AI_boostfov;
    public float AI_shootfov;
    public float AI_shootrange;
	// Use this for initialization
	public new void Start () {
        GetComponent<GenericShip>().Start();

        player = GameObject.FindWithTag("Player");

        Inventory = new List<WeaponSlot>() { AllWeapons.Instance.EnemyLaser };
    }

    // Update is called once per frame
    public new void Update () {
        GetComponent<GenericShip>().Update();

        //Simple movement towards the player and shooting
        #region
        float ang = Vector3.SignedAngle(transform.forward, player.transform.position - transform.position, Vector3.up);
            float dis = Vector3.Distance(player.transform.position, transform.position);

            bool bst, finetoshoot, shouldboost, shouldshoot, finetoboost;

            finetoshoot = (Mathf.Abs(ang) <= AI_shootfov);
            shouldshoot = (dis <= AI_shootrange);

            finetoboost = (Mathf.Abs(ang) <= AI_boostfov);
            shouldboost = (dis >= AI_boostrange);

            if (finetoboost && shouldboost)
            {
                bst = true;
            }
            else
            {
                bst = false;
            }

            engine.MoveThing(player.transform.position, true, bst, true);

            if (finetoshoot && shouldshoot)
            {
                Shoot(0);
            }
        #endregion

    }
}
