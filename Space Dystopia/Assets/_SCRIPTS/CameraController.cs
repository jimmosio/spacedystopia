﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;
    private Vector3 offset;

    public float MaxDistance;
    public float MinDistance;
    public float RefSpeed;
    public float angle;
    public float DistanceDamping = 1.0f;
    public float ShockDamping = 1.0f;
    public float ShockUpToSpeedValue = 20.0f;
    public float Mi = 0.75f;

    [HideInInspector]
    public float targetdistance;
    [HideInInspector]
    public float currdistance;
    [HideInInspector]
    public float _dv_, prevv, shock_strength = 0.0f;

    //Place camera at a certain distance (and angle)
    void PlaceCamera(float dist)
    {
        float _x = 0.0f;
        float _y = dist * Mathf.Sin(Mathf.Deg2Rad * angle);
        float _z = -dist * Mathf.Cos(Mathf.Deg2Rad * angle);
        offset = new Vector3(_x, _y, _z);

        transform.position = player.transform.position + offset;

        transform.rotation = Quaternion.Euler(angle, 0.0f, 0.0f);
    }

	// Use this for initialization
	void Start () {
        offset = transform.position - player.transform.position;

        PlaceCamera(MaxDistance);
	}
	
	// Update is called once per frame
	void Update () {
        //Placing the camera where the player is + zooming in/out because of current speed and shocks
        #region
        float v = player.GetComponent<Rigidbody>().velocity.magnitude;
        float dv = v - prevv;

        //This part makes no sense at all.
        //It's just a bunch of semi-random mathematical expressions. I know what I did but I can't bother explaining it in comment form
        //If you want to change it tell me -jimmosio
        #region
        _dv_ = Mathf.Pow(Mathf.Abs(dv) / Time.deltaTime, Mi);
        float uts = Mathf.Min(_dv_ / ShockUpToSpeedValue, 1.0f);
        shock_strength = Mathf.Max(uts * _dv_, shock_strength);

        targetdistance = MinDistance + (MaxDistance - MinDistance) / ((v + shock_strength) / RefSpeed + 1.0f);
        currdistance = Vector3.Distance(player.transform.position + offset, player.transform.position);
        float delta = Mathf.Abs(targetdistance - currdistance) / RefSpeed;

        float t;
        if (Mathf.Approximately(delta, 0.0f))
        {
            t = 1.0f;

        }
        else
        {
            t = DistanceDamping * Time.deltaTime / delta;

        }

        float nextdistance = Mathf.Lerp(currdistance, targetdistance, t);


        float s = ShockDamping * Time.deltaTime;
        shock_strength = Mathf.Lerp(shock_strength, _dv_, s);
        #endregion

        PlaceCamera(nextdistance);

        prevv = v;
        #endregion

    }
}
