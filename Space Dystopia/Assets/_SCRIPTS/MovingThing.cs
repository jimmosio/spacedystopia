﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Engine
{
    [HideInInspector]
    public GameObject gameObject;

    public float TurnRate;
    public float ShipTilt;
    public float ThrustForce;

    public float BoostForce;
    public float BoostDepressuring;
    public float BoostRepressuring;
    public float BoostMinRepressuring;

    [HideInInspector]
    public float boostMaxPressure;
    [HideInInspector]
    public float boostCurrPressure;
    public float boostMaxCooldown;
    [HideInInspector]
    public float boostCurrCooldown;
    [HideInInspector]
    public float boostPressureInc;
    [HideInInspector]
    public float boostPressureRatio;

    public void MoveThing(Vector3 target, bool thrusting, bool boosting, bool ForceOnPlane)
    {
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        Transform tr = gameObject.GetComponent<Transform>();
        //Turn towards target
        Quaternion rotation = Quaternion.LookRotation(target - tr.position);
        float TurnRateAlpha = Quaternion.Angle(tr.rotation, rotation);
        float t = Time.deltaTime * TurnRate / TurnRateAlpha;
        tr.rotation = Quaternion.Slerp(tr.rotation, rotation, t);

        //Tilt around the Z axis
        float ang = Vector3.SignedAngle(tr.forward, target - tr.position, Vector3.up);
        float sine = Mathf.Sin(Mathf.Deg2Rad * ang);
        float a = 1.5f;
        float U = -Mathf.Sign(sine) * Mathf.Pow(Mathf.Abs(sine), a) * ShipTilt;
        tr.eulerAngles = new Vector3(tr.eulerAngles.x, tr.eulerAngles.y, U);

        //Thrust
        float k = 2.0f;
        float minangle = 180.0f;
        float thrustturncoeff = Mathf.Clamp(Mathf.Pow(Mathf.Max(1 - TurnRateAlpha / minangle, 0.0f), k), 0.0f, 1.0f);
        float thrst;

        if (thrusting)
        {
            thrst = ThrustForce;

        }
        else
        {
            //if not thrusting, the ship will just rotate towards the target position
            thrst = 0.0f;
        }

        //Boost input

        //if boost is on cooldown, it doens't recharge
        if (boostCurrCooldown != 0.0f)
        {
            boostPressureInc = 0.0f;

        }
        //if not on cooldown, check for input
        else if (boosting && thrusting)
        {
            //additional thrust
            float bstfrc = BoostForce * boostPressureRatio;
            thrst = thrst + bstfrc;

            //in case the player is boosting this value is used
            boostPressureInc = -BoostDepressuring * thrustturncoeff;
        }

        thrst = thrst * thrustturncoeff;

        rb.AddForce(tr.forward * thrst);
        
        if (ForceOnPlane)
        {
            ForceOnXZPlane();

        }

    }

    public void ForceOnXZPlane()
    {
        Transform tr = gameObject.GetComponent<Transform>();
        //Force ship at y=0

        tr.position = new Vector3(tr.position.x, 0.0f, tr.position.z);

        //Force ship angle

        tr.eulerAngles = new Vector3(0.0f, tr.eulerAngles.y, tr.eulerAngles.z);
    }

    public void HaltBoosting()
    {
        if (boostCurrCooldown == 0.0f)
            boostCurrCooldown = boostMaxCooldown;

    }

}

public class MovingThing : MonoBehaviour {
    public Engine engine;

    // Use this for initialization
    public virtual void Start () {
        engine.gameObject = gameObject;

        engine.boostMaxPressure = 1.0f;
        engine.boostCurrPressure = engine.boostMaxPressure;

    }

    // Update is called once per frame
    public virtual void Update () {
        //BOOST
        #region
        //tick cooldown of boost
        engine.boostCurrCooldown = Mathf.Max(engine.boostCurrCooldown - Time.deltaTime, 0.0f);

            //re(de)pressure boost
            engine.boostCurrPressure = Mathf.Clamp(engine.boostCurrPressure + engine.boostPressureInc * Time.deltaTime, 0.0f, engine.boostMaxPressure);
            //if boost depleted, put it on cooldown
            if ((engine.boostCurrPressure == 0.0f) && (engine.boostPressureInc < 0.0f))
            {
                engine.HaltBoosting();
            }

            engine.boostPressureRatio = engine.boostCurrPressure / engine.boostMaxPressure;
            //repressuring rate. in case nothing interesting happens this value is used
            engine.boostPressureInc = Mathf.Max(engine.BoostRepressuring * engine.boostPressureRatio, engine.BoostMinRepressuring);
        #endregion

    }

    public virtual void FixedUpdate()
    {
        
    }
}
