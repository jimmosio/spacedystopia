﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerShip : GenericShip {

    // Use this for initialization
    
    public new void Start () {
        GetComponent<GenericShip>().Start();
        Inventory = new List<WeaponSlot>() { AllWeapons.Instance.StandardLaser };
    }
    
    private bool boostinput;
    private bool thrustinput;

    [HideInInspector]
    public Vector3 targetpos = Vector3.zero;
    [HideInInspector]
    public Vector3 directioninput = Vector3.zero;

    // Update is called once per frame
    public new void Update ()
    {
        GetComponent<GenericShip>().Update();
        if (Input.GetButton("Shoot"))
            Shoot(0);

        //MOVEMENT OF THE SHIP
        #region
            //Get input
            #region
            float horin = Input.GetAxis("Horizontal");
            float verin = Input.GetAxis("Vertical");

            float mxin = Input.GetAxis("Mouse X");
            float myin = Input.GetAxis("Mouse Y");

            bool UsingJoystick = ((!Mathf.Approximately(horin, 0.0f)) || (!Mathf.Approximately(verin, 0.0f)));
            bool UsingMouse = (((!Mathf.Approximately(mxin, 0.0f)) || (!Mathf.Approximately(myin, 0.0f))) && !UsingJoystick); //Joystick over mouse

            //Mouse input: project mouse position onto game plane
            if (UsingMouse)
            {
                //Get where mouse is lying on the XZ plane
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                // create a plane at 0,0,0 whose normal points to +Y:
                Plane hPlane = new Plane(Vector3.up, Vector3.zero);
                // Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
                float distance = 0.0f;
                // if the ray hits the plane...
                //Vector3 targetpos = Vector3.zero;
                if (hPlane.Raycast(ray, out distance))
                {
                    // get the hit point:
                    targetpos = ray.GetPoint(distance);
                    directioninput = targetpos - transform.position;
                }
            }

            //Joystick input: depending on joystick output, turn around
            if (UsingJoystick)
            {
                float r = 256.0f;
                float horinR = horin * r;
                float verinR = verin * r;

                directioninput = new Vector3(horinR, 0.0f, verinR);
                targetpos = transform.position + directioninput;

            }

            if (Input.GetButtonUp("Boost"))
            {
                boostinput = false;
                engine.HaltBoosting();

            }
            else if (Input.GetButton("Boost"))
            {
                boostinput = true;
            }
            else
            {
                boostinput = false;
            }


            if (Input.GetButton("Thrust"))
            {
                thrustinput = true;

            }
            else
            {
                thrustinput = false;
                boostinput = false;

            }
            #endregion

            //Actually move the ship
            #region
            Vector3 goalpos = transform.position + directioninput;
                    engine.MoveThing(goalpos, thrustinput, boostinput, true);
                }

        #endregion

        #endregion

    //FixedUpdate is called before any physics frame
    private new void FixedUpdate()
    {
    GetComponent<GenericShip>().FixedUpdate();

    }
}
