﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnPars
{
    public float SpawnMaxCooldown = 3.0f;
    [HideInInspector]
    public float SpawnCurrCooldown;

    public int MaxSpawnsAtOnce = 6;

    public float SpawnMinAddedDistance = 5.0f;
    public float SpawnMaxAddedDistance = 10.0f;

    public GameObject EnemyToSpawn;

    public void SpawnUpdate()
    {
        GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");

        //If can spawn more enemies, tick cooldown and spawn (cooldown pauses when enemies are too much)
        if (gos.Length < MaxSpawnsAtOnce)
        {
            SpawnCurrCooldown = Mathf.Max(SpawnCurrCooldown - Time.deltaTime, 0.0f);

            if (SpawnCurrCooldown == 0.0f)
            {
                Vector3 playerpos = GameObject.FindWithTag("Player").transform.position;

                //From the perspective of the screen, generate a random angle, get an edge point and transform it into unity coordinates
                #region

                //Random angle, distance between screen center and edge point with that angle
                #region
                float ang = Random.Range(0.0f, 2 * Mathf.PI);

                float a = Screen.width / 2.0f;
                float b = Screen.height / 2.0f;

                float b_over_a = b / a;

                float ABStanang = Mathf.Abs(Mathf.Tan(ang));

                float screendis;
                if (ABStanang <= b_over_a)
                {
                    screendis = a / Mathf.Abs(Mathf.Cos(ang));
                }
                else
                {
                    screendis = b / Mathf.Abs(Mathf.Sin(ang));
                }
                #endregion

                //Position of the edge point, transformed into world coordinates
                #region
                float screenx = a + screendis * Mathf.Cos(ang);
                float screeny = b + screendis * Mathf.Sin(ang);
                Vector3 spawnpos_screen = new Vector3(screenx, screeny, 0.0f);

                //Get where edge point is lying on the XZ plane
                Ray ray = Camera.main.ScreenPointToRay(spawnpos_screen);
                // create a plane at 0,0,0 whose normal points to +Y:
                Plane hPlane = new Plane(Vector3.up, Vector3.zero);
                // Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
                float distance = 0.0f;
                // if the ray hits the plane...
                Vector3 spawnpos = Vector3.zero;
                if (hPlane.Raycast(ray, out distance))
                {
                    // get the hit point:
                    spawnpos = ray.GetPoint(distance);

                }
                #endregion

                //Move the spawn position further away by a random distance
                #region
                float D = Vector3.Distance(spawnpos, playerpos);
                float inc = Random.Range(SpawnMinAddedDistance, SpawnMaxAddedDistance);
                spawnpos = Vector3.LerpUnclamped(playerpos, spawnpos, 1 + inc / D);
                Quaternion rot = Quaternion.LookRotation(playerpos - spawnpos, Vector3.up);
                #endregion

                #endregion

                //Finally, spawn enemy
                #region
                Object.Instantiate(EnemyToSpawn, spawnpos, rot);

                #endregion

                Debug.Log("Spawned enemy at distance-angle (" + 
                    (Vector3.Distance(spawnpos, playerpos)).ToString("F0") + 
                    ", " +
                    (Vector3.SignedAngle(playerpos, spawnpos - playerpos, Vector3.up)).ToString("F0") + ")" +
                    " and screendis-ang (" + screendis.ToString("F0") + ", " + (ang * Mathf.Rad2Deg).ToString("F0") + ")");

                SpawnCurrCooldown = SpawnMaxCooldown;
            }
        }
    }
}

public class EnemySpawner : MonoBehaviour {
    public SpawnPars SpawnParameters;

	// Use this for initialization
	void Start () {
        SpawnParameters.SpawnCurrCooldown = SpawnParameters.SpawnMaxCooldown;

    }
	
	// Update is called once per frame
	void Update () {
        SpawnParameters.SpawnUpdate();
    }
}
