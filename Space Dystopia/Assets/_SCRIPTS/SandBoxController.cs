﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SandBoxController : MonoBehaviour {
    private GameObject playerObject;
    private GameObject cameraObject;
    private Rigidbody playerRB;
    private PlayerShip playerShip;

    public Text SandBoxText;

	// Use this for initialization
	void Start () {
        playerObject = GameObject.FindWithTag("Player");

        cameraObject = GameObject.FindWithTag("MainCamera");

        playerRB = playerObject.GetComponent<Rigidbody>();

        playerShip = playerObject.GetComponent<PlayerShip>();

    }
	
	// Update is called once per frame
	void Update () {
        //restart
		if (Input.GetKeyDown(KeyCode.R))
        {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        }
	}

    private void FixedUpdate()
    {
        //sand box text
        //CameraController cmr = cameraObject.GetComponent<CameraController>();
        //if (!playerObject) return;

        #region
        Vector3 mousepos;
        float playermousedis = 0.0f;
        //Get where mouse is lying on the XZ plane
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // create a plane at 0,0,0 whose normal points to +Y:
        Plane hPlane = new Plane(Vector3.up, Vector3.zero);
        // Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
        float distance = 0.0f;
        // if the ray hits the plane...
        //Vector3 targetpos = Vector3.zero;
        if (hPlane.Raycast(ray, out distance))
        {
            // get the hit point:
            mousepos = ray.GetPoint(distance);
            playermousedis = Vector3.Distance(playerObject.transform.position, mousepos);

        }
        #endregion

        int vel,bst,hp;
        vel = Mathf.CeilToInt(playerRB.velocity.magnitude * 15.625f);
        bst = Mathf.CeilToInt(playerShip.engine.boostCurrPressure / playerShip.engine.boostMaxPressure * 100.0f);
        hp = Mathf.CeilToInt(playerShip.meatsack.Health);

        SandBoxText.text =
            "M1: move; M2: shoot; Z: boost; R: restart" +
            "\nVelocity: " + vel.ToString() +
            "\nBoost Pressure: " + bst.ToString() + "%" +
            "\nHealth: " + hp.ToString() +
            "\nMouse-player distance: " + playermousedis.ToString("F0") + 
            "\nEnemies: " + GameObject.FindGameObjectsWithTag("Enemy").Length.ToString();

    }
}
